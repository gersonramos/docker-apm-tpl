#!/usr/bin/env bash

args=""

for var in "$@"
do
    args="$args $var"
done

bash -c "docker-compose exec web ${args}"
